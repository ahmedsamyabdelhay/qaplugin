const colors = {
  BLACK: "#000",
  WHITE: "#FFF",
  PURPLE: "#8127dF",
  DODGER_BLUE: "#428AF8",
  SILVER: "#BEBEBE",
  TORCH_RED: "#D61d71",
  KISHKA: "#E5E4E6",
  LIGHT_GRAY: "#D3D3D3",
  DAZE_BLACK: "rgba(255,255,255,0.4)",
  TRANSPARENT: "transparent"
};

export default {
  
};
