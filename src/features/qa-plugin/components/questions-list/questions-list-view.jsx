import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";

const useStyles = makeStyles(theme => ({
  container: {
    height: "100%",
    width: "25%",
    maxHeight: window.innerHeight,
  },
  list: {
    width: "100%",
    minWidth: "25%",
    height: window.innerHeight - 230,
    overflow: 'auto',
    position: 'relative',
    backgroundColor: "grey"
  },
  button: {
      width: "100%",
      height: 50,
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: "black",
      color: "white"
  },
  textarea: {
    width: "100%",
    height: 150,
  }
}));

const QuestionsList = props => {
  const classes = useStyles();

  return (
    <div className={classes.container}>
      <List className={classes.list} component="nav" aria-label="main mailbox folders">
        {props.questions.map((question, index) => {
          return (
            <ListItem
              button
              key={index}
              selected={props.selectedIndex === index}
              onClick={event => props.onQuestionClicked(event, index)}
            >
              <ListItemText primary={question} />
            </ListItem>
          );
        })}
      </List>
      <textarea className={classes.textarea} value={props.questionText} onChange={props.onQuestionTextInputChange} />
      <button className={classes.button} onClick={props.onCreateQuestionBtnClick}>Create Question</button>
    </div>
  );
};

export default QuestionsList;
