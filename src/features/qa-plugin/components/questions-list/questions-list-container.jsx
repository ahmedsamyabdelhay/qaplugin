import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { actions as QuestionsListActions } from "./questions-list-redux";
import QuestionsList from "./questions-list-view";
import UtilFunctions from "../../utils/util-functions";
import "firebase/database";

const QuestionsListContainer = ({
  questionsListActions,
  appReady,
  firebaseApp,
  ...props
}) => {
  // ------------------- States ------------------- //
  const [questionText, setQuestionText] = React.useState(""); // Indicate current state of question input
  const [selectedQuestion, setSelectedQuestion] = React.useState(null); // Indicate current state of question input
  // --------------------------------------------- //

  // ----------------- Callbacks ------------------ //
  const onCreateQuestionBtnClick = React.useCallback(() => {
    if (questionText !== "") {
      console.log("creating question now ...");
      console.log(questionText);
      const questionId = firebaseApp
        .database()
        .ref()
        .child("questions")
        .push().key;
      firebaseApp
        .database()
        .ref()
        .child(`questions/${questionId}`)
        .set(questionText).then(() => {
            setQuestionText("");
        });
    }
  });

  const onQuestionTextInputChange = React.useCallback(event => {
    setQuestionText(event.target.value);
  });

  const onQuestionClicked = React.useCallback((event, index) => {
    setSelectedQuestion(index);
    questionsListActions.displayQuestion(
      UtilFunctions.convertJSONToArray(props.questions)[index]
    );
  });
  // ---------------------------------------------- //

  // ------------------- Effects ------------------- //
  React.useEffect(() => {
    questionsListActions.initiateFetchQuestions();
  }, [questionsListActions, appReady]);
  // ---------------------------------------------- //

  return (
    <QuestionsList
      questions={UtilFunctions.convertJSONToArray(props.questions).reverse()}
      selectedQuestion={selectedQuestion}
      questionText={questionText}
      error={props.error}
      onCreateQuestionBtnClick={onCreateQuestionBtnClick}
      onQuestionTextInputChange={onQuestionTextInputChange}
      onQuestionClicked={onQuestionClicked}
    />
  );
};

export default connect(
  state => {
    const { questions, error } = state.questionsList;
    const { appReady, firebase } = state.app;
    return {
      firebaseApp: firebase,
      appReady: appReady,
      questions: questions,
      error: error
    };
  },
  dispatch => ({
    questionsListActions: bindActionCreators(QuestionsListActions, dispatch)
  })
)(QuestionsListContainer);
