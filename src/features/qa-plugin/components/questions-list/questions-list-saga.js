import { eventChannel, END } from "redux-saga";
import { take, put, select, takeLatest } from "redux-saga/effects";
import {
  actions as QuestionsListActions,
  questionsListActionTypes
} from "./questions-list-redux";
import "firebase/database";

/**
 * Questions listener for async real time updates
 *
 * @since      0.0.1
 *
 * @access     private
 *
 * @see        initiateFetchQuestionsSaga
 *
 * @yield      questions on every update from database
 */
function* initiateFetchQuestions() {
  // Get firebase database instance
  const firebaseApp = yield select(state => state.app.firebase);
  const database = firebaseApp.database();

  // Create a subscriber to firebase real time updates
  const channel = new eventChannel(emitter => {
    console.log("Subscribing into firebase database now ...");
    const listener = database.ref("questions").on("value", snapshot => {
      emitter({ data: snapshot.val() || {} });
    });
    // Clean up
    return () => {
      listener.off();
      emitter(END);
    };
  });

  // Pause the task until the channel emits a signal and dispatch an action in the store
  while (true) {
    const { data } = yield take(channel);
    yield put(QuestionsListActions.initiateFetchQuestionsSuccess(data));
  }
}

/**
 * Interceptor
 *
 * Intercepts  initiateFetchQuestions action
 *
 * @since      0.0.1
 *
 * @access     private
 *
 * @yield      initiateFetchQuestions saga
 */
const initiateFetchQuestionsSaga = function*() {
  yield takeLatest(
    questionsListActionTypes.initiateFetchQuestions,
    initiateFetchQuestions
  );
};

export default [initiateFetchQuestionsSaga];
