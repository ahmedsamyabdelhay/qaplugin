import { createAction, handleActions } from "redux-actions";

export const questionsListActionTypes = {
  initiateFetchQuestions: "@@questionsList/INITIATE_FETCH_QUESTIONS",
  initiateFetchQuestionsSuccess:
    "@@questionsList/INITIATE_FETCH_QUESTIONS_SUCCESS",
  initiateFetchQuestionsError: "@@questionsList/INITIATE_FETCH_QUESTIONS_ERROR",
  displayQuestion: "@@questionsList/DISPLAY_QUESTION"
};

// Creates action based on component action types to be dispatched
export const actions = Object.keys(questionsListActionTypes).reduce(
  (res, key) =>
    Object.assign(res, { [key]: createAction(questionsListActionTypes[key]) }),
  {}
);

// Default state of the component reducer
const defaultState = {
  question: null,
  questions: {},
  error: null
};

// Reducer that returns the state to connected components
export default handleActions(
  {
    [questionsListActionTypes.initiateFetchQuestionsSuccess]: (
      state,
      action
    ) => {
      let returnState = {
        ...state,
        questions: action.payload
      };
      return returnState;
    },
    [questionsListActionTypes.initiateFetchQuestionsError]: (state, action) => {
      let returnState = {
        ...state,
        ...action.payload
      };
      return returnState;
    },
    [questionsListActionTypes.displayQuestion]: (state, action) => {
      let returnState = {
        ...state,
        question: action.payload
      };
      return returnState;
    }
  },
  defaultState
);
