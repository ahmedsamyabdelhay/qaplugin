import React from "react";
import Styles from "./question-display-styles";

const QuestionDisplay = props => {
  return (
    <Styles.Container>
      <h1>{props.question}</h1>
    </Styles.Container>
  );
};

export default QuestionDisplay;
