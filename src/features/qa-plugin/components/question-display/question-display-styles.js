import styled from "styled-components";

const Container = styled.div`
  text-align: center;
  height: 100%;
  min-width: 75%;
  background-color: black;
  color: white;
`;

export default {
  Container
};
