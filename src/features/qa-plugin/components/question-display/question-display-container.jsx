import React from "react";
import { connect } from "react-redux";
import QuestionDisplay from "./question-display-view";

const questionDisplayContainer = props => {
  return <QuestionDisplay question={props.question} />;
};

export default connect(state => {
  const { question } = state.questionsList;
  return {
    question: question
  };
})(questionDisplayContainer);
