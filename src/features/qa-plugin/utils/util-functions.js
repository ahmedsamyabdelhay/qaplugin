export default {
  convertJSONToArray: json => convertJSONToArrayImp(json)
};

/**
 * Convert JSON data into array of values
 *
 * @since      0.0.1
 *
 * @access     public
 *
 * @param      {Object} json        JSON object.
 *
 * @return     {Array}  dataArray   Array of the JSON data values (Keys are now indices)
 */
function convertJSONToArrayImp(json) {
  // Create an array container for the conversion result
  var dataArray = [];
  // Fill the container array with the child objects
  for (var property in json) {
    // Skip loop if the property is from prototype
    if (!json.hasOwnProperty(property)) {
      continue;
    }
    // Otherwise push the value to the array
    var value = json[property];
    dataArray.push(value);
  }
  return dataArray;
}
