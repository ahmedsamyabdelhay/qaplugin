import React from "react";
import QuestionsList from "./components/questions-list/";
import QuestionDisplay from "./components/question-display/";
import Styles from "./qa-plugin-styles";

const QAPlugin = props => {
  return (
    <Styles.Container>
      <QuestionDisplay />
      <QuestionsList />
    </Styles.Container>
  );
};

export default QAPlugin;
