import styled from "styled-components";

const Container = styled.div`
  height: 100%;
  text-align: center;
  display: flex;
  flex-direction: row;
  background-color: black;
`;

export default {
  Container
};
