import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actions as AppActions } from './app-redux';
import App from './app-view';

const AppContainer = ({
  appActions,
  ...props
}) => {
  // ------------------- Effects ------------------- //
  React.useEffect(() => {
      appActions.readyApp();
  }, [appActions]);
  // ---------------------------------------------- //

  return props.appReady ? <App /> : <h1>Server is down. Please come back later</h1>;
};

export default connect(
  (state) => {
    const { appReady } = state.app;
    return {
        appReady: appReady
    };
  },
  dispatch => ({
    appActions: bindActionCreators(AppActions, dispatch)
  })
)(AppContainer);
