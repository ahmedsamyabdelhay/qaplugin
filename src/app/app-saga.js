import { takeLatest, put } from "redux-saga/effects";
import { actions as AppActions, appActionTypes } from "./app-redux";
import firebase from 'firebase/app';
/**
 * Initialize pre-app stuff
 *
 * @since      0.0.1
 *
 * @access     private
 *
 * @see        readyAppSaga
 *
 * @yield      Result of initializations
 */
const readyApp = function*() {
  let state = {};
  try {
    console.log("initializing Firebase");
    const app = firebase.initializeApp({
      apiKey: "AIzaSyD68gr3Cm1UtFNRb_WfXbGTKYpA6rYtH4U",
      authDomain: "qaplugin-6130a.firebaseapp.com",
      databaseURL: "https://qaplugin-6130a.firebaseio.com",
      projectId: "qaplugin-6130a",
      storageBucket: "qaplugin-6130a.appspot.com",
      messagingSenderId: "577326369019",
      appId: "1:577326369019:web:3e4a57f3ee315bf771c85e",
      measurementId: "G-48LZM7YVHQ"
    });
    state.appReady = true;
    state.firebase = app
    yield put(AppActions.readyAppSuccess(state));
  } catch (error) {
    console.log("Error ready app: " + error);
    state.appReady = false;
    yield put(AppActions.readyAppError(state));
  }
};

/**
 * Interceptor
 *
 * Intercepts  readyApp action
 *
 * @since      0.0.1
 *
 * @access     private
 *
 * @yield      readyApp saga
 */
const readyAppSaga = function*() {
  yield takeLatest(appActionTypes.readyApp, readyApp);
};

export default [readyAppSaga];
