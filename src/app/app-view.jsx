import React from 'react';
import { IntlProvider } from 'react-intl';
import English from '../configs/locales/en.json';
import Arabic from '../configs/locales/ar.json';
import QAPlugin from '../features/qa-plugin/';

const messages = {
  'en': English,
  'ar': Arabic
}
const language = navigator.language.split(/[-_]/)[0];

const App = () => {
  return (
      <IntlProvider locale={language} messages={messages[language]}>
        <QAPlugin />
      </IntlProvider>
  );
};

export default App;
