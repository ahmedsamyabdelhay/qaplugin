import { createAction, handleActions } from 'redux-actions';

export const appActionTypes = {
    readyApp: "@@app/READY_APP",
    readyAppSuccess: "@@app/READY_APP_SUCCESS",
    readyAppError: "@@app/READY_APP_ERROR"
}

// Creates action based on component action types to be dispatched
export const actions = Object.keys(appActionTypes).reduce(
  (res, key) =>
    Object.assign(res, { [key]: createAction(appActionTypes[key]) }),
  {}
);

// Default state of the component reducer
const defaultState = {
  appReady: false,
  firebase: null
};

// Reducer that returns the state to connected components
export default handleActions(
  {
    [appActionTypes.readyAppSuccess]: (state, action) => {
      let returnState = {
        ...state,
        ...action.payload
      };
      return returnState;
    }
  },
  defaultState
);
