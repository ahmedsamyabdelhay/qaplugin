import React from "react";
import { Provider } from "react-redux";
import App from "./app/";
import Store from "./store";
import "./App.css";

const AppRoot = () => {
  return (
    <Provider store={Store}>
      <App />
    </Provider>
  );
};

export default AppRoot;