import { applyMiddleware, createStore } from 'redux';
import Reducers from './redux';
import Saga from './saga';

// Boiler plate to create store for global state management
const configureStore = () => {
  const middleWares = applyMiddleware(Saga.sagaMiddleware);
  const store = createStore(Reducers, middleWares);
  Saga.sagaMiddleware.run(Saga.rootSaga);
  return store;
};

export default configureStore();
