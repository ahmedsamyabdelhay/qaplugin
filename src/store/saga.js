import { all, fork } from 'redux-saga/effects';
import createSagaMiddleware from 'redux-saga';
import AppInitSagas from '../app/app-saga';
import QuestionsListSagas from '../features/qa-plugin/components/questions-list/questions-list-saga';

// Boiler plate to initiate sagas
const rootSaga = function*() {
  let sagas = []
  sagas = sagas.concat(AppInitSagas, QuestionsListSagas)
  yield all(sagas.map(saga => fork(saga)));
};

export default { rootSaga, sagaMiddleware: createSagaMiddleware() };
