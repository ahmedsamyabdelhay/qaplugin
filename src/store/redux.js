import { combineReducers } from 'redux';
import AppInitReducer from '../app/app-redux';
import QuestionsListReducer from '../features/qa-plugin/components/questions-list/questions-list-redux';

export const reducersStateObject = {
  app: AppInitReducer,
  questionsList: QuestionsListReducer
};

export default combineReducers(reducersStateObject);
